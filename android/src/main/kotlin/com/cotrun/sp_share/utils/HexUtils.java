package com.cotrun.sp_share.utils;

public class HexUtils {
    public static final HexUtils INSTANCE = new HexUtils();

    private HexUtils(){

    }

    public byte[] hexStringToBytes(String str) {
        str = str.toUpperCase();
        int length = str.length() / 2;
        if (str != null) {
            char[] toCharArray = str.toCharArray();
            byte[] bArr = new byte[length];
            for (int i = 0; i < length; i++) {
                int i2 = i * 2;
                bArr[i] = (byte) (charToByte(toCharArray[i2 + 1]) | (charToByte(toCharArray[i2]) << 4));
            }
            return bArr;
        }else{
            return null;
        }

    }

    public byte charToByte(char c){
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    public String bytesToHexString(byte[] bArr){
        StringBuilder stringBuilder = new StringBuilder("");
        if ((bArr.length == 0 ? 1 : null) != null) {
            return "";
        }
        for (byte b : bArr) {
            String toHexString = Integer.toHexString(b & 255);
            if (toHexString.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(toHexString);
        }
        String stringBuilder2 = stringBuilder.toString();
        return stringBuilder2;
    }
}

package com.cotrun.sp_share.utils;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.util.List;

public class SystemUtils {
    private static String name;
    public static String getAllAppNamesPackages(PackageManager pm) {
        List<PackageInfo> list = pm
                .getInstalledPackages(PackageManager.GET_UNINSTALLED_PACKAGES);

        StringBuffer stringBuffer = new StringBuffer();


        for (PackageInfo packageInfo : list) {
            // 获取到应用所在包的名字,即在AndriodMainfest中的package的值。
            String packageName = packageInfo.packageName;
            stringBuffer.append(packageName + ",");
        }
        name = stringBuffer.substring(0, stringBuffer.length() - 1).toString();

        return name;
    }
}

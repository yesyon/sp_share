package com.cotrun.sp_share.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import kotlin.text.Charsets;

public class AESUtils {
    public static AESUtils INSTANCE = new AESUtils();
    private static String algorithm = "AES";
    private static String transformation = "AES/ECB/PKCS5Padding";
    public static String encrypt(String str,String str2){
        try{
            Cipher instance = Cipher.getInstance(transformation);
            byte[] bytes = str2.getBytes(Charsets.UTF_8);
            instance.init(1,new SecretKeySpec(bytes,algorithm));
            byte[] bytes2 = str.getBytes(Charsets.UTF_8);
            bytes2 = instance.doFinal(bytes2);
            HexUtils hexUtils = HexUtils.INSTANCE;
            return  hexUtils.bytesToHexString(bytes2);
        }catch (Exception e){
//            L.info(e.getMessage()+e);
            e.printStackTrace();
            return null;
        }

    }

    public static String decrypt(String str, String str2) {
        try {
            Cipher instance = Cipher.getInstance(transformation);
            byte[] bytes = str2.getBytes(Charsets.UTF_8);
            instance.init(2, new SecretKeySpec(bytes, algorithm));
            byte[] doFinal = instance.doFinal(HexUtils.INSTANCE.hexStringToBytes(str));
            //Intrinsics.checkExpressionValueIsNotNull(doFinal, "encrypt");
            return new String(doFinal, Charsets.UTF_8);
        }catch (Exception e){
            e.printStackTrace();
//            log.info(e.getMessage());
            return null;
        }

    }

}

package com.cotrun.sp_share


import android.app.Activity
import android.content.Context
import androidx.annotation.NonNull
import com.cotrun.sp_share.utils.AESUtils
import com.cotrun.sp_share.utils.LogUtils
import com.cotrun.sp_share.utils.ShareWeChatUtils.Companion.WXshareURl
import com.cotrun.sp_share.utils.SystemUtils
import com.cotrun.sp_share_example.*
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result


/** SpSharePlugin */
class SpSharePlugin: FlutterPlugin, MethodCallHandler, ActivityAware {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private lateinit var context: Context
  private lateinit var activity: Activity


  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "sp_share")
    channel.setMethodCallHandler(this)
    context = flutterPluginBinding.applicationContext

  }

//  companion object {
//    fun registerWith(registrar:Registrar) {
//      val channel = MethodChannel(registrar.messenger(), "flutter_plugin_name")
//      channel.setMethodCallHandler(FlutterMapboxTurnByTurnPlugin())
//    }
//  }

  override fun onMethodCall(@NonNull methodCall: MethodCall, @NonNull result: Result) {
    if (methodCall.method == "getPlatformVersion") {
      result.success("Android ${android.os.Build.VERSION.RELEASE}")
    }
//    LogUtils.logE(" ---> send :进来这里没有啊88888。")
    when (methodCall.method) {
      aesSetChannel -> {
        result?.success(AESUtils.encrypt(methodCall.argument<String>("str")!!, methodCall.argument<String>("key")!!))
      }
      aesGetChannel -> {
        result?.success(AESUtils.decrypt(methodCall.argument<String>("str")!!, methodCall.argument<String>("key")!!))
      }
      PackageManagerChannel -> {
        result?.success(SystemUtils.getAllAppNamesPackages(this.context.packageManager))
      }
      shareWeChatChannel -> {  // 分享微信
        result?.success(WXshareURl(
                this.context, methodCall.argument<Boolean>("isScene")!!,
                methodCall.argument<String>("shareUrl")!!,
                methodCall.argument<String>("shareTitle")!!,
                methodCall.argument<String>("shareDesc")!!,
                methodCall.argument<String>("shareThumbnail")!!))
      }
      else -> {
        result?.notImplemented()
      }
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    activity = binding.activity;
  }

  override fun onDetachedFromActivityForConfigChanges() {
//    TODO("Not yet implemented")
  }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
//    TODO("Not yet implemented")
  }

  override fun onDetachedFromActivity() {
//    TODO("Not yet implemented")
  }

}





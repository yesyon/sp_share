package com.cotrun.sp_share_example

/**
 * @author：HLQ_Struggle
 * @date：2020/6/27
 * @desc：
 */

/**
 * 通道名称
 */
const val channelName = "HLQStruggle"

/**
 * 检测命中数量 > 0 代表可采用命中宿主方案借壳分享
 */
const val aesSetChannel = "aesSet"
const val aesGetChannel = "aesGet"

const val PackageManagerChannel = "PackageManager"

/**
 * 分享微信
 */
const val shareWeChatChannel = "shareWeChat"

/**
 * 分享微信消息会话
 */
const val shareWeChatSession = false

/**
 * 分享微信朋友圈
 */
const val shareWeChatLine = true

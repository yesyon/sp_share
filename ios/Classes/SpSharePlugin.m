#import "SpSharePlugin.h"
#if __has_include(<sp_share/sp_share-Swift.h>)
#import <sp_share/sp_share-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "sp_share-Swift.h"
#endif

@implementation SpSharePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftSpSharePlugin registerWithRegistrar:registrar];
}
@end

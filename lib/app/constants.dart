/// @date 2020-06-27
/// @author HLQ_Struggle
/// @desc 常量类

/// 通道名称
const String channelName = 'HLQStruggle';

const String aesSet = 'aesSet';
const String aesGet = 'aesGet';
const String PackageManager = 'PackageManager';

/// 分享微信
const String shareWeChat = 'shareWeChat';

import 'dart:async';

import 'package:flutter/services.dart';

import 'app/constants.dart';

class SpShare {
  static const MethodChannel _channel =
      const MethodChannel('sp_share');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> platAesSet(String str,String key) async {
    final String aesCode = await _channel.invokeMethod(aesSet,{'str':str,'key':key});
    return aesCode;
  }

  static Future<String> platAesGet(String str,String key) async {
    final String aesCode = await _channel.invokeMethod(aesGet,{'str':str,'key':key});
    return aesCode;
  }

  static Future<String> PackageName() async {
    final String aesCode = await _channel.invokeMethod(PackageManager);
    return aesCode;
  }

  static Future<bool> platShare(bool isScene,String shareTitle,String shareDesc,String shareUrl,String shareThumbnail) async {
    var aesCode = await _channel.invokeMethod(shareWeChat,{
      'isScene': isScene,
      'shareTitle': shareTitle,
      'shareDesc': shareDesc,
      'shareUrl': shareUrl,
      'shareThumbnail':shareThumbnail
    });
    return aesCode;
  }

}
